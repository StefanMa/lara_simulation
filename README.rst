sila2_simulation
=============================

This package is designed to provide a simulation of all devices present in the Greifswald laboratory.
The simulation contains detailed digital twins of the devices which (mainly) respond the same way
as their real counterparts to commands.

Additionally these devices have ASCII art outputs which are combined to a visual representation of the lab
in the console. It updates every 0.5 seconds using the curses module.

The simulation is meant to be used to test the communication of SiLA servers with their devices as well as
testing environment for scheduling whole experiments or supervising them with third party tools that support SiLA.

Currently the simulations contains:

* Cytomat (or two of them)
* Centrifuge
* Plate Hotel
* Robotic Arm
* Plate Reader
More implementations will come:
* Pipetting Robot
* Barcode Reader
* Warning Light
* Dispenser
* Gripping Tool

Installation
------------
1.  You need to install SiLA first. Follow the instructions in README in sila_python root directory.

2.  The simulation_package is currently only contained in the feature/greifswald_lara_devices_implementation branch in directory sila_tools/sila2_simulation

    Installation of this package is done the usual way:

    .. code-block:: bash

        cd sila_python/sila_tools/sila2_simulation
        pip install .
3.  Do either 3.a or 3b depending on whether you also want to simulate the usage of C#-dll's from python

    3a    1.  Compile the dummy-C#-wrapper. In Linux for example by

            .. code-block:: bash

                cd  sila_python/implementations/logistic_managers/HelpingHand/cs_wrapper/
                mcs /target:library dummy_wrapper.cs

        2.  Install a fitting .NET Framework if you are in Linux.

        3.  Set the flags

            .. code-block:: python

                SIMULATE_WINDOOF = True

            in both *sila_python/implementations/logistic_managers/HelpingHand/arm_protocol.py*
            and *sila_python/implementations/logistic_managers/PlateStorageHotel/hotel_protocol.py*


    3b  Set the flags

            .. code-block:: python

                SIMULATE_WINDOOF = False

            in both *sila_python/implementations/logistic_managers/HelpingHand/arm_protocol.py*
            and *sila_python/implementations/logistic_managers/PlateStorageHotel/hotel_protocol.py*

Usage
-----
You need several console tabs to fully use the simulation

1.
    Go into the source directory:

    .. code-block:: bash

        cd sila_python/sila_tools/sila2_simulation/sila2_simulation

2.  To start the SiLA servers with their simulated devices run

    .. code-block:: bash

        python zookeeper.py

    You can control which servers are started by commenting in/out
    the corresponding lines in __init__() of *zookeeper.py*

    In this tab you can observe all outputs of the servers. You can have the visual output of the devices in this tab
    by setting the *visualize* parameter of the run()-command in the main routine in the very and of *zookeeper.py*.

3.  The other way of having the visual output is running. This has the advantage of simultaneously being able to
    observe the server output.

    .. code-block:: bash

        python visualize_from_json.py

4.  From a third console tab you can either run

    .. code-block:: bash

        python zoo_visitor.py

    to run a fixed sequence of SiLA commands, or create a python (or ipython) console and run

    .. code-block:: python

        from zoo_visitor import init_clients
        arm_client, centrifuge_client, hotel_client, cytomat_client, cytomat2_client, reader_client = init_clients()

    Some of those clients will be *None* in case they could not connect to a running server.
    Then you can manually command your simulated SiLA devices and try out stuff.

5.  Examples for custom SiLA commands:

    .. code-block:: python

        centrifuge_client.coverController_client.OpenCover()
        centrifuge_client.rotationController_client.SetSpeed(6000)
        centrifuge_client.rotationController_client.StartRotating()
        # the intern names are Rotanta_Rotor, Carousel, Cytomat1550_1, Cytomat1550_2, VarioskanLUX
        arm_client.robotController_client.MovePlate("Carousel", 2, "Cytomat1550_1", 0)
        stack, pos = cytomat_client.stackerController_client.Get_NextFreePosition()
        cytomat_client.stackerController_client.UnloadContainerFromStacker(0, 4)
