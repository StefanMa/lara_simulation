from time import sleep
from arm_server.server import ArmServer
from arm_server.features.client import Client as ArmClient
from barcoderead_server.server import BCRServer

# hacky, but its quite useful to use fixed ports
ports_by_name = dict(
    arm=50051, centrifuge=50052, hotel=50053, cytomat=50054, cytomat2=50055, reader=50056, bc_reader=50057,
    pipetter=50058
)

server_type = dict(
    arm=ArmServer,
    bc_reader=BCRServer,
)


class ZooKeeper2:
    def __init__(self, devices=[], simulation=True):
        self.servers = {name: None for name in ports_by_name}
        try:
            for name in devices:
                self.servers[name] = server_type[name](simulation=simulation)
                self.servers[name].start_insecure('0.0.0.0', ports_by_name[name], enable_discovery=True)

            self.virtual_devices = {name: server.hardware_interface for name, server in self.servers.items()
                                    if server is not None}
            if simulation:
                if 'hotel' in devices:
                    for pos in range(5):
                        self.virtual_devices['hotel'].add(0, pos)
                for vd in self.virtual_devices.values():
                    if 'inner_state' in vd.__dir__():
                        vd.inner_state.reset()
            print("Please press ctrl-c to stop...")
            while True:
                if simulation:
                    for name, device in self.virtual_devices.items():
                        with open(f"visual_{name}.txt", 'w') as outstream:
                            port_string = f"port: {ports_by_name[name]}\n"
                            outstream.write(port_string + str(device))
                sleep(.5)
        finally:
            # stop the new servers
            for name in ports_by_name:
                if self.servers[name] is not None:
                    self.servers[name].stop()


if __name__ == '__main__':
    zookeeper = ZooKeeper2(
        devices=[
            'arm',
            'bc_reader',
        ],
        simulation=True
    )
