import curses
import sys
from time import sleep
from sila2_simulation.lab_visualization import LabVisualization
from deviceTest import DeviceTest as dt

sys.path.append("/home/stefan/workspace/sila_legacy/implementations/incubators_shakers/ThermoCytomat2")
from cytomat_test import CytomatTest
from ThermoCytomat2_client import ThermoCytomat2Client
from ThermoCytomat2_server import ThermoCytomat2Server
sys.path.append("/home/stefan/workspace/sila_legacy/implementations/logistic_managers/PlateStorageHotel")
from hotel_test import HotelTest
from PlateStorageHotel_client import PlateStorageHotelClient
from PlateStorageHotel_server import PlateStorageHotelServer
sys.path.append("/home/stefan/workspace/sila_legacy/implementations/logistic_managers/HelpingHand")
from HelpingHand_client import HelpingHandClient
from arm_test import ArmTest
sys.path.append("/home/stefan/workspace/sila_legacy/implementations/centrifuges/HettichRotanta460R")
from centrifuge_test import CentrifugeTest
from HettichRotanta460R_client import HettichRotanta460RClient
sys.path.append("/home/stefan/workspace/sila_legacy/implementations/photometers_platereaders/ThermoVarioskanLUX")
from plateReaderTest import PlateReaderTest
from ThermoVarioskanLUX_client import ThermoVarioskanLUXClient
from ThermoVarioskanLUX_server import ThermoVarioskanLUXServer
from arm_server.server import ArmServer
from arm_server.features.client import Client as ArmClient
from barcoderead_server.server import BCRServer


# hacky, but its quite useful to use fixed ports
ports_by_name = dict(
    arm=50051, centrifuge=50052, hotel=50053, cytomat=50054, cytomat2=50055, reader=50056, bc_reader=50057,
    pipetter=50058
)

robot_interaction_features = dict(
    centrifuge=('Rotanta_Rotor', 'RobotInteractionServiceRotanta'),
    hotel=('Carousel', 'RobotInteractionService'),
    cytomat=('Cytomat1550_1', 'RobotInteractionServiceCy'),
    cytomat2=('Cytomat1550_2', 'RobotInteractionServiceCy'),
    reader=('VarioskanLUX', 'RobotInteractionServicePR'),
    bc_reader=('BarCodeReaderMS3', 'RobotInteractionService'),
    pipetter=('Bravo', 'RobotInteractionService'),
)


class ZooKeeper2:
    def __init__(self, devices=[], simulation=True):
        self.servers = {name: None for name in ports_by_name}
        try:
            if 'arm' in devices:
                self.servers['arm'] = ArmServer(simulation=simulation)
                self.servers['arm'].start_insecure('127.0.0.1', ports_by_name['arm'], enable_discovery=True)
            if 'centrifuge' in devices:
                self.servers['centrifuge'] = CentrifugeTest()
            if 'hotel' in devices:
                self.servers['hotel'] = PlateStorageHotelServer(
                    dt.create_args(port=ports_by_name['hotel']), simulation_mode=simulation, block=False
                )
            if 'cytomat' in devices:
                self.servers['cytomat'] = ThermoCytomat2Server(
                    dt.create_args(port=ports_by_name['cytomat']), simulation_mode=simulation, block=False
                )
            if 'reader' in devices:
                self.servers['reader'] = ThermoVarioskanLUXServer(
                    dt.create_args(port=ports_by_name['reader']), simulation_mode=simulation, block=False
                )
            if 'bc_reader' in devices:
                self.servers['bc_reader'] = BCRServer("com", simulation=simulation)
                self.servers['bc_reader'].start_insecure('127.0.0.1', ports_by_name['bc_reader'], enable_discovery=True)

            self.virtual_devices = {name: server.hardware_interface for name, server in self.servers.items()
                                    if server is not None}
            if simulation:
                for vd in self.virtual_devices.values():
                    vd.inner_state.reset()
                if 'hotel' in devices:
                    for pos in range(5):
                        self.virtual_devices['hotel'].add(0, pos)
            print("Please press ctrl-c to stop...")
            while True:
                if simulation:
                    for name, device in self.virtual_devices.items():
                        with open(f"visual_{name}.txt", 'w') as outstream:
                            port_string = f"port: {ports_by_name[name]}\n"
                            outstream.write(port_string + str(device))
                sleep(.5)
        finally:
            # stop the new servers
            for name in ['arm', 'bc_reader', 'pipetter']:
                if self.servers[name] is not None:
                    self.servers[name].stop()
            # stop the old servers
            for name in ['reader', 'cytomat', 'cytomat2', 'centrifuge', 'hotel']:
                if self.servers[name] is not None:
                    self.servers[name].kill_command = True

client_classes = dict(
    arm=("", HelpingHandClient, ""),
    centrifuge=('Rotanta_Rotor', HettichRotanta460RClient, 'robotInteractionServiceRotanta_client'),
    hotel=('Carousel', PlateStorageHotelClient, 'robotInteractionService_client'),
    cytomat=('Cytomat1550_1', ThermoCytomat2Client, 'robotInteractionServiceCy_client'),
    cytomat2=('Cytomat1550_2', ThermoCytomat2Client, 'robotInteractionServiceCy_client'),
    reader=('VarioskanLUX', ThermoVarioskanLUXClient, 'robotInteractionServicePR_client'),
)


class ZooKeeper(LabVisualization):
    def __init__(self):
        super(ZooKeeper, self).__init__()
        self.device_by_name = dict(
            arm=ArmTest(),
            #centrifuge=CentrifugeTest(),
            reader=PlateReaderTest(),
            hotel=HotelTest(),
            cytomat=CytomatTest(),
            #cytomat2=CytomatTest(),
        )
        for name, device in self.device_by_name.items():
            self.add_device(device, port=ports_by_name[name])

    def custom_setup(self):
        self.get_in_real_mode()
        self.device_by_name["arm"].server.register_clients()
        #self.set_up_arm_protocols()
        if 'hotel' in self.device_by_name:
            virtual_hotel = self.device_by_name['hotel'].server.hardware_interface
            virtual_hotel.inner_state.reset()
            for pos in range(5):
                virtual_hotel.add(0, pos)

    def get_in_real_mode(self):
        for test in self.device_by_name.values():
            test.server.hardware_interface.get_in_real_mode()

    # this should be redundant after the RobotInteractionService is implemented for all devices
    def set_up_arm_protocols(self):
        arm_protocol = self.device_by_name['arm'].server.hardware_interface
        for name, (lara_name, client_class, sub_client) in client_classes.items():
            if name in self.device_by_name and not name == "arm":
                arm_protocol.register_access_client(
                    lara_name, client_class(server_port=ports_by_name[name]).__getattribute(sub_client)
                )


if __name__ == '__main__':
    #zoo_keeper = ZooKeeper()
    #zoo_keeper.run(visualize=False)
    zoo_keeper = ZooKeeper2(devices=[
        #'arm',
        'hotel',
        'cytomat',
        'reader',
        #'bc_reader',
    ], simulation=True)
