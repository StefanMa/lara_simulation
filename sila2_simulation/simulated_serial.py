from threading import Timer, Thread
from time import sleep
import re
import logging
from abc import ABC, abstractmethod


class SimulatedSerial(ABC):
    fast_mode = True

    def get_in_fast_mode(self):
        self.fast_mode = True

    def get_in_real_mode(self):
        self.fast_mode = False

    @abstractmethod
    def get_answer(self, command, terminator, encoding='utf-8', max_wait=2):
        pass

    # We need to have the same functions as ComSerial, but leave them empty
    def write_str(self, output_str: str, encoding: str = 'utf-8'):
        pass

    def close(self):
        pass

    def open_first_free_port(self, baudrate=9600) -> bool:
        return True

    def open(self, port: str, baudrate=115200) -> bool:
        return True
