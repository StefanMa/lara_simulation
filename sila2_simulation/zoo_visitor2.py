import time
from sila2_simulation.zookeeper2 import ports_by_name
from sila2.client import SilaClient

from arm_server.features.client import Client as ArmClient
from barcoderead_server.generated.client import Client as BCRClient

num_plates = 2
server_ip = "127.0.0.1"
method_name = "single_read"
shaking_time = 2
#server_ip = "141.53.22.61"
#method_name = "210924_varioskan_SPabs96_600_660_3"
#method_name = "211001_varioskan_single_well_600"
#shaking_time = 15

lara_names = dict(
    centrifuge='Rotanta_Rotor',
    hotel='Carousel',
    cytomat='Cytomat1550_1',
    cytomat2='Cytomat1550_2',
    reader='VarioskanLUX',
    bc_reader='BarCodeReaderMS3',
    pipetter='Bravo',
)

def setup_interaction(interaction_devices=[], connect_barcode_reader=False):
    # connect barcode reader with arm
    arm_client = SilaClient(server_ip, ports_by_name['arm'])
    if connect_barcode_reader:
        arm_client.BarcodeReadingService.RegisterBarcodeReader(
            "BarCodeReaderMS3", ports_by_name['bc_reader'])
    # connect robot interaction clients
    for name in interaction_devices:
        arm_client.DeviceInteractionService.RegisterDevice(lara_names[name], ports_by_name[name])

def init_clients(server_ip='127.0.0.1', devices=[]):
    clients = []
    for name in devices:
        try:
            client = SilaClient(server_ip, ports_by_name[name])
        except:
            client = None
        clients.append(client)
    return clients

def finish_observable_command(cmd, args):
    cmd_info = cmd(*args)
    time.sleep(.5)
    print(cmd_info)
    print(cmd_info.status)
    print(cmd_info.status.name)
    # 0,1 are waiting/running   2/3 are success/error
    while cmd_info.status.value < 2:
        end = '\r' if cmd_info.status.value < 2 else '\n'
        print(f"time_remaining:{cmd_info.estimated_remaining_time.seconds}"
              f": {cmd_info.status.name}", end=end)
        time.sleep(.1)
    print(cmd_info, cmd_info.get_responses())
    return cmd_info

def finish_with_error_timeout(cmd, args, timeout):
    start = time.time()
    while True:
        try:
            result = finish_observable_command(cmd, args)
            if result.status.value == 2:
                return result.get_responses()
        except:
            pass
        if time.time() - start > timeout:
            print("TIMEOUT")
            raise result.get_responses()
        else:
            time.sleep(1)

def run_protocol():
    start = time.time()
    print('creating clients...')
    arm_client: ArmClient
    arm_client, hotel_client, cytomat_client, reader_client = init_clients(
        server_ip=server_ip, devices=['arm', 'hotel', 'cytomat', 'reader']
    )
    print("setting up interaction")
    setup_interaction(interaction_devices=['hotel', 'cytomat', 'reader'], connect_barcode_reader=True)
    print("starting process")

    cytomat_client.TemperatureController.ControlTemperature(273.15 + 37)
    # put plates into cytomat
    for i in range(num_plates):
        finish_observable_command(cmd=arm_client.RobotController.MovePlate,
                                  args=[("Carousel", i), ("Cytomat1550_1", i), 'unlidded'])
    # shake and incubate
    cytomat_client.ShakingController.SetShakingFrequency(700, 0)
    # wait until cytomat has finished loading
    finish_with_error_timeout(cmd=cytomat_client.ShakingController.StartShaking,
                              args=[0], timeout=60)
    # shake for 2 seconds
    time.sleep(shaking_time)

    # read the absorbance
    for i in range(num_plates-1):
        #reader_client.dataProvider_client.SetDataDir(r"")
        finish_observable_command(cmd=arm_client.RobotController.MovePlate,
                                  args=[("Cytomat1550_1", i), ("VarioskanLUX", 0), 'unlidded'])

        finish_with_error_timeout(cmd=reader_client.ProtocolExecutionService.ExecuteProtocol,
                                  args=[method_name], timeout=10)
        # store it in the second column
        finish_observable_command(cmd=arm_client.RobotController.MovePlate,
                                  args=[("VarioskanLUX", 0), ("Carousel", i+25), 'unlidded'])

    # put the other plate directly into carousel
    finish_observable_command(cmd=arm_client.RobotController.MovePlate,
                              args=[("Cytomat1550_1", 1), ("Carousel", 26), 'unlidded'])
    cytomat_client.ShakingController.StopShaking(-1)

    print(f"Protocol took {time.time()-start} seconds.")

if __name__ == '__main__':
    run_protocol()