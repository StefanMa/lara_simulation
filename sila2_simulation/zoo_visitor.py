import time
from sila2lib.error_handling.client_err import SiLAClientError
from sila2_simulation.zookeeper import ports_by_name, client_classes


num_plates = 2
server_ip = "localhost"
method_name = "single_read"
shaking_time = 2
#server_ip = "141.53.22.61"
#method_name = "210924_varioskan_SPabs96_600_660_3"
#method_name = "211001_varioskan_single_well_600"
#shaking_time = 15


def init_clients(ip='localhost', ignore=[]):
    clients = []
    for name, (lara_name, client_class, subclass) in client_classes.items():
        if lara_name in ignore:
            clients.append(None)
        else:
            try:
                client = client_class(server_ip=ip, server_port=ports_by_name[name])
            except:
                client = None
            clients.append(client)
    return clients


def finish_observable_command(cmd, info, args):
    uuid = cmd(*args)
    status = info(uuid.commandExecutionUUID)
    while status.is_active():
        n = status.next()
        end = '\r' if n.CommandStatus.Name(n.commandStatus) in ['waiting', 'running'] else '\n'
        print(f"time_remaining:{n.estimatedRemainingTime.seconds}/"
              f"{n.estimatedRemainingTime.seconds+n.updatedLifetimeOfExecution.seconds}"
              f": {n.CommandStatus.Name(n.commandStatus)}", end=end)
        time.sleep(.1)


def finish_with_error_timeout(cmd, info, args, timeout):
    start = time.time()
    while True:
        try:
            finish_observable_command(cmd, info, args)
            return
        # wait until the device is ready
        except SiLAClientError as dsb:
            if time.time() - start > timeout:
                print("TIMEOUT")
                raise dsb
            else:
                time.sleep(1)


def run_protocol():
    start = time.time()
    arm_client, centrifuge_client, hotel_client, cytomat_client, cytomat2_client, reader_client =\
        init_clients(ip=server_ip, ignore=['Rotanta_Rotor', 'Cytomat1550_2'])
    # heat up
    cytomat_client.temperatureController_client.ControlTemperature(273.15 + 37)
    # put plates into cytomat
    for i in range(num_plates):
        finish_observable_command(cmd=arm_client.robotController_client.MovePlate,
                                  info=arm_client.robotController_client.MovePlate_Info,
                                  args=["Carousel", i, "Cytomat1550_1", i+16])
    # shake and incubate
    cytomat_client.shakingController_client.SetShakingFrequency(700)
    # wait until cytomat has finished loading
    finish_with_error_timeout(cmd=cytomat_client.shakingController_client.StartShaking,
                              info=cytomat_client.shakingController_client.StartShaking_Info,
                              args=[0],
                              timeout=60)
    # shake for 2 seconds
    time.sleep(shaking_time)

    # read the absorbance
    for i in range(num_plates-1):
        #reader_client.dataProvider_client.SetDataDir(r"")
        finish_observable_command(cmd=arm_client.robotController_client.MovePlate,
                                  info=arm_client.robotController_client.MovePlate_Info,
                                  args=["Cytomat1550_1", i+16, "VarioskanLUX", 0])

        finish_with_error_timeout(cmd=reader_client.protocolExecutionService_client.ExecuteProtocol,
                                  info=reader_client.protocolExecutionService_client.ExecuteProtocol_Info,
                                  args=[method_name],
                                  timeout=10)
        # store it in the second column
        finish_observable_command(cmd=arm_client.robotController_client.MovePlate,
                                  info=arm_client.robotController_client.MovePlate_Info,
                                  args=["VarioskanLUX", 0, "Carousel", i+25])

        finish_observable_command(cmd=cytomat_client.shakingController_client.StopShaking,
                                  info=cytomat_client.shakingController_client.StopShaking_Info,
                                  args=[1])
    # put the other plate directly into carousel
    finish_observable_command(cmd=arm_client.robotController_client.MovePlate,
                              info=arm_client.robotController_client.MovePlate_Info,
                              args=["Cytomat1550_1", 1+16, "Carousel", 26])
    cytomat_client.shakingController_client.StopShaking()
    # move plates into centrifuge
    #for i in range(num_plates):
        #next_free = centrifuge_client.stackerController1d_client.Get_NextFreePosition()
     #   finish_observable_command(cmd=arm_client.robotController_client.MovePlate,
      #                            info=arm_client.robotController_client.MovePlate_Info,
                                  #args=["Cytomat1550_1", i+1, "Rotanta_Rotor", next_free])
        #                          args=["Cytomat1550_1", i, "Rotanta_Transfer", i])
    # centrifuge
    #centrifuge_client.rotationController_client.SetSpeed(6000)
    #centrifuge_client.rotationController_client.StartRotating()
    #sleep(10)
    #centrifuge_client.rotationController_client.StopRotating()
    #centrifuge_client.coverController_client.OpenCover()
    #centrifuge_client.stackerController1d_client.NullifyOccupancy()
    print(f"Protocol took {time.time()-start} seconds.")


if __name__ == '__main__':
    run_protocol()
